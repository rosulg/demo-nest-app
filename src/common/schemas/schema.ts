import * as Joi from '@hapi/joi';

export const idSchema = Joi.object({ id: Joi.number().required() });

export const paginationSchema = Joi.object({
  limit: Joi.number().optional(),
  page: Joi.number().optional(),
});
