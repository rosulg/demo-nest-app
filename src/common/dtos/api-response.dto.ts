import { ApiProperty } from '@nestjs/swagger';

export abstract class ApiResponseDto<D, MD>
  implements Readonly<ApiResponseDto<D, MD>> {
  @ApiProperty()
  data: D;
  @ApiProperty()
  metadata: MD;
}
