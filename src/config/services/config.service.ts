import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { readFileSync } from 'fs';
import * as dotenv from 'dotenv';
import { Logger } from '@nestjs/common';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import { join } from 'path';
import * as Joi from '@hapi/joi';

export class ConfigService {
  private readonly logger = new Logger(ConfigService.name);
  private readonly envConfig;

  constructor() {
    const env = process.env.NODE_ENV || 'development';
    const file = readFileSync(`./config.${env}.env`, 'utf-8');
    const envConfig = dotenv.parse(file);
    this.validate(envConfig);
    this.envConfig = envConfig;
  }

  private validate(config): void {
    const environmentSchema = Joi.object({
      ENV: Joi.string().valid('test', 'development', 'production'),
      // Database
      POSTGRES_HOST: Joi.string().required(),
      POSTGRES_PORT: Joi.string().required(),
      POSTGRES_USER: Joi.string().required(),
      POSTGRES_PASSWORD: Joi.string().required(),
      POSTGRES_DB: Joi.string().required(),
    });
    const { error } = environmentSchema.validate(config);
    if (error) {
      this.logger.error('Failed to validate config values');
      process.exit(1);
    }
  }

  private getValue(key: string): string {
    const value = this.envConfig[key];
    if (!value) {
      throw new Error(`missing env.${key} value`);
    }
    return value;
  }

  get typeOrmConfig(): TypeOrmModuleOptions {
    return {
      type: 'postgres',
      host: this.getValue('POSTGRES_HOST'),
      port: parseInt(this.getValue('POSTGRES_PORT')),
      username: this.getValue('POSTGRES_USER'),
      password: this.getValue('POSTGRES_PASSWORD'),
      database: this.getValue('POSTGRES_DB'),
      namingStrategy: new SnakeNamingStrategy(),
      entities: [join(__dirname, '../../**', '*.entity.{ts,js}')],
      ssl: false, // this.isProduction();
    };
  }

  private isProduction(): boolean {
    return this.getValue('ENV') === 'production';
  }
}
