import { createConnection, ConnectionOptions, QueryRunner } from 'typeorm';
import { ConfigService } from '../config/services/config.service';
import { Order } from '../order/models/order.entity';
import { Product } from '../product/models/product.entity';
import { Decimal } from 'decimal.js';
import { OrderProduct } from '../order/models/order-product.entity';
import { v4 as uuidv4 } from 'uuid';

function randInt(from: number, to: number): number {
  return Math.floor(Math.random() * to) + from;
}

function calculateOrderSum(
  orderProducts: OrderProduct[],
  calculatePrice: (price, vat, quantity) => Decimal,
): Decimal {
  return orderProducts.reduce((acc, op) => {
    const price = calculatePrice(op.product.price, op.product.vat, op.quantity);
    return acc.plus(price);
  }, new Decimal(0));
}

function calculateTotal(orderProducts: OrderProduct[]): Decimal {
  const calculatePrice = (price, vat, quantity) =>
    new Decimal(price).mul(new Decimal(1).plus(vat)).times(quantity);
  return calculateOrderSum(orderProducts, calculatePrice);
}

function calculateSubtotal(orderProducts: OrderProduct[]): Decimal {
  const calculatePrice = (price, vat, quantity) =>
    new Decimal(price).times(quantity);
  return calculateOrderSum(orderProducts, calculatePrice);
}

async function createProducts(queryRunner: QueryRunner): Promise<Product[]> {
  const products = new Array(20).fill(null).map((e, i) => {
    const product = new Product();
    product.name = `Flower-${uuidv4()}`;
    product.ean = `EE${Math.floor(Math.random() * 10000000)}`;
    product.stock = randInt(1, 10);
    const price = Math.floor(Math.random() * (1000 - 100) + 100) / 100;
    product.price = `${1 + price}`;
    return product;
  });

  return queryRunner.manager.save(products);
}

async function createOrders(queryRunner: QueryRunner): Promise<void> {
  const name = 'John Doe';

  const products = await queryRunner.manager.find(Product);
  const lastOrder = await queryRunner.manager.findOne(Order, {
    order: { id: 'DESC' },
  });

  const data = new Array(50).fill(null).map((el, i) => {
    const order = new Order();
    order.id = lastOrder && lastOrder.id ? lastOrder.id + i + 1 : 0;
    order.name = name;

    const start = randInt(0, products.length / 2);
    const end = randInt(products.length / 2 + 2, products.length);
    const pickedProducts = products.slice(start, end);

    const orderProducts = pickedProducts.map(prod => {
      const orderProduct = new OrderProduct();
      orderProduct.order = order;
      orderProduct.product = prod;
      orderProduct.quantity = randInt(1, prod.stock);
      return orderProduct;
    });

    order.subtotal = calculateSubtotal(orderProducts).toFixed();
    order.total = calculateTotal(orderProducts).toFixed();
    return { order, orderProducts };
  });

  const orders = [];
  const orderProducts = [];

  data.forEach(data => {
    orders.push(data.order);
    orderProducts.push(...data.orderProducts);
  });

  await queryRunner.manager.save(orders);
  await queryRunner.manager.save(orderProducts);
}

async function run(): Promise<void> {
  const config = new ConfigService();

  const connection = await createConnection({
    ...config.typeOrmConfig,
    debug: true,
  } as ConnectionOptions);

  const queryRunner = connection.createQueryRunner();
  await queryRunner.startTransaction();

  try {
    console.log('...generating products');
    await createProducts(queryRunner);

    console.log('...generating orders');
    await createOrders(queryRunner);

    await queryRunner.commitTransaction();
  } catch (err) {
    await queryRunner.rollbackTransaction();

    throw err;
  } finally {
    await queryRunner.release();
  }
}

run().catch(error => console.error('error', error));
