import { Controller, Get } from '@nestjs/common';
import { ApiResponseDto } from './common/dtos/api-response.dto';
import { ApiResponse } from '@nestjs/swagger';

@Controller()
export class AppController {
  @ApiResponse({
    description: 'Base controller',
    type: ApiResponseDto,
    status: 200,
  })
  @Get()
  getHello(): ApiResponseDto<string, any> {
    return {
      data: 'There is an API here!',
      metadata: {},
    };
  }
}
