import { ApiProperty } from '@nestjs/swagger';
import { Product } from '../models/product.entity';

export class ProductDto implements Readonly<ProductDto> {
  @ApiProperty({ required: true })
  id: number;

  @ApiProperty({ required: true })
  name: string;

  @ApiProperty({ required: true })
  ean: string;

  @ApiProperty({ required: true })
  stock: number;

  @ApiProperty({ required: true })
  price: string;

  @ApiProperty({ required: true })
  vat: string;

  static from(entity: Product): ProductDto {
    const dto = new ProductDto();
    dto.id = entity.id;
    dto.name = entity.name;
    dto.ean = entity.ean;
    dto.stock = entity.stock;
    dto.price = entity.price;
    dto.vat = entity.vat;
    return dto;
  }

  static fromMany(entities: Product[]): ProductDto[] {
    return entities.map(entity => ProductDto.from(entity));
  }

  toEntity(): Product {
    const entity = new Product();
    entity.name = this.name;
    entity.ean = this.ean;
    entity.stock = this.stock;
    entity.price = this.price;
    entity.vat = this.vat;
    return entity;
  }
}
