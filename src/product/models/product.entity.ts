import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  Column,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  ean: string;

  @Column('int')
  stock: number;

  @Column({ type: 'decimal', precision: 10, scale: 4 })
  price: string;

  @Column({ type: 'decimal', precision: 10, scale: 4, default: 0.2 })
  vat: string;

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string;

  @DeleteDateColumn()
  deletedAt: string;
}
