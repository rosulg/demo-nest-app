import { Controller, Get, Param, Query, UsePipes } from '@nestjs/common';
import { ProductService } from '../services/product.service';
import { ApiResponseDto } from '../../common/dtos/api-response.dto';
import { ProductDto } from '../dtos/product.dto';
import { JoiValidationPipe } from '../../common/pipes/joi-validation.pipe';
import { idSchema, paginationSchema } from '../../common/schemas/schema';
import { ApiResponse, ApiQuery, ApiParam } from '@nestjs/swagger';

@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @ApiResponse({
    description: 'Returns all products',
    type: ApiResponseDto,
    status: 200,
  })
  @ApiQuery({ name: 'page', required: false, type: Number })
  @ApiQuery({ name: 'limit', required: false, type: Number })
  @UsePipes(new JoiValidationPipe(paginationSchema))
  @Get('list')
  async findAll(@Query() query): Promise<ApiResponseDto<ProductDto[], any>> {
    if (query.limit && query.page) {
      const { products, count } = await this.productService.findAndCount(
        query.limit,
        (query.page - 1) * query.limit,
      );
      return {
        data: products,
        metadata: {
          total: count,
        },
      };
    }
    const products = await this.productService.findAll();
    return {
      data: products,
      metadata: {
        total: products.length,
      },
    };
  }

  @ApiResponse({
    description: 'Returns single product',
    type: ApiResponseDto,
    status: 200,
  })
  @UsePipes(new JoiValidationPipe(idSchema))
  @ApiParam({
    name: 'id',
    required: true,
    type: Number,
    description: 'Product id',
  })
  @Get(':id')
  async find(@Param() params): Promise<ApiResponseDto<ProductDto, any>> {
    const product = await this.productService.findOne(params.id);
    return {
      data: product,
      metadata: {},
    };
  }

  @ApiResponse({
    description:
      'Returns products that have been bought alongside with the specified product id.',
    type: ApiResponseDto,
    status: 200,
  })
  @UsePipes(new JoiValidationPipe(idSchema))
  @ApiParam({
    name: 'id',
    required: true,
    type: Number,
    description: 'Product id',
  })
  @Get('related/:id')
  async findRelated(
    @Param() params,
  ): Promise<ApiResponseDto<ProductDto[], any>> {
    const products = await this.productService.findRelated(params.id);
    return {
      data: products,
      metadata: {},
    };
  }
}
