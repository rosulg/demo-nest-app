import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from '../models/product.entity';
import { ProductDto } from '../dtos/product.dto';

interface Data {
  products: ProductDto[];
  count: number;
}

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
  ) {}

  async findAll(): Promise<ProductDto[]> {
    const products = await this.productRepository.find();
    return ProductDto.fromMany(products);
  }

  async findOne(id: number): Promise<ProductDto> {
    const product = await this.productRepository.findOne(id);
    return ProductDto.from(product);
  }

  async findRelated(id: number): Promise<ProductDto[]> {
    const products = await this.productRepository.manager.query(
`
      -- get all orders that contain specified product
      WITH orders AS (
        SELECT op.order_id
        FROM order_product AS op
        WHERE op.product_id = $1
      )
      -- select products that are in orders and are not the specified product
      SELECT 
        p.*,
        (
          SELECT count(*)
          FROM order_product
          WHERE product_id = p.id
        ) AS popularity
      FROM 
        orders AS o LEFT JOIN
        order_product AS op ON o.order_id = op.order_id LEFT JOIN
        product p ON op.product_id = p.id
      WHERE 
        p.id != $1
      GROUP BY p.id
      ORDER BY popularity DESC;
    `,
      [id],
    );

    return ProductDto.fromMany(products as Product[]);
  }

  async findAndCount(limit: number, offset: number): Promise<Data> {
    const [products, count] = await this.productRepository.findAndCount({
      take: limit,
      skip: offset,
    });
    return {
      products: ProductDto.fromMany(products),
      count,
    };
  }

  async create(dto: ProductDto): Promise<ProductDto> {
    const product = dto.toEntity();
    const createdProduct = await this.productRepository.save(product);
    return ProductDto.from(createdProduct);
  }
}
