import { Module } from '@nestjs/common';
import { OrderService } from './services/order.service';
import { OrderProductService } from './services/order-product.service';
import { OrderController } from './controllers/order.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './models/order.entity';
import { OrderProduct } from './models/order-product.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Order, OrderProduct])],
  providers: [OrderService, OrderProductService],
  controllers: [OrderController],
})
export class OrderModule {}
