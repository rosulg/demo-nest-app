import { ApiProperty } from '@nestjs/swagger';
import { Order } from '../models/order.entity';

export class OrderDto implements Readonly<OrderDto> {
  @ApiProperty({ required: true })
  id: number;

  @ApiProperty({ required: true })
  name: string;

  @ApiProperty({ required: true })
  subtotal: string;

  @ApiProperty({ required: true })
  total: string;

  static from(entity: Order): OrderDto {
    const dto = new OrderDto();
    dto.id = entity.id;
    dto.name = entity.name;
    dto.subtotal = entity.subtotal;
    dto.total = entity.total;
    return dto;
  }

  static fromMany(entities: Order[]): OrderDto[] {
    return entities.map(entity => OrderDto.from(entity));
  }

  toEntity(): Order {
    const entity = new Order();
    entity.name = this.name;
    entity.subtotal = this.subtotal;
    entity.total = this.total;
    return entity;
  }
}
