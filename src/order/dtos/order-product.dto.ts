import { ApiProperty } from '@nestjs/swagger';
import { Order } from '../models/order.entity';
import { Product } from '../../product/models/product.entity';
import { OrderProduct } from '../models/order-product.entity';

export class OrderProductDto implements Readonly<OrderProductDto> {
  @ApiProperty({ required: true })
  id: number;

  @ApiProperty({ required: true })
  orderId: number;

  @ApiProperty({ required: true })
  productId: number;

  @ApiProperty({ required: true })
  quantity: number;

  static from(entity: OrderProduct): OrderProductDto {
    const dto = new OrderProductDto();
    dto.id = entity.id;
    dto.orderId = entity.order.id;
    dto.productId = entity.product.id;
    dto.quantity = entity.quantity;
    return dto;
  }

  static fromMany(entities: OrderProduct[]): OrderProductDto[] {
    return entities.map(entity => OrderProductDto.from(entity));
  }

  toEntity(): OrderProduct {
    const order = new Order();
    order.id = this.orderId;

    const product = new Product();
    product.id = this.productId;

    const entity = new OrderProduct();
    entity.order = order;
    entity.product = product;
    entity.quantity = this.quantity;
    return entity;
  }
}
