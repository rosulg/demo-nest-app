import { Controller, Get, Param, Query, UsePipes } from '@nestjs/common';
import { OrderService } from '../services/order.service';
import { ApiResponseDto } from '../../common/dtos/api-response.dto';
import { OrderDto } from '../dtos/order.dto';
import { JoiValidationPipe } from '../../common/pipes/joi-validation.pipe';
import { idSchema, paginationSchema } from '../../common/schemas/schema';
import { ApiParam, ApiQuery, ApiResponse } from '@nestjs/swagger';

@Controller('order')
export class OrderController {
  constructor(private readonly orderService: OrderService) {}

  @ApiResponse({
    description: 'Returns all orders',
    type: ApiResponseDto,
    status: 200,
  })
  @ApiQuery({ name: 'page', required: false, type: Number })
  @ApiQuery({ name: 'limit', required: false, type: Number })
  @UsePipes(new JoiValidationPipe(paginationSchema))
  @Get('list')
  async findAll(@Query() query): Promise<ApiResponseDto<OrderDto[], any>> {
    if (query.limit && query.page) {
      const { orders, count } = await this.orderService.findAndCount(
        query.limit,
        (query.page - 1) * query.limit,
      );
      return {
        data: orders,
        metadata: {
          total: count,
        },
      };
    }
    const orders = await this.orderService.findAll();
    return {
      data: orders,
      metadata: {
        total: orders.length,
      },
    };
  }

  @ApiResponse({
    description: 'Returns single order',
    type: ApiResponseDto,
    status: 200,
  })
  @UsePipes(new JoiValidationPipe(idSchema))
  @ApiParam({
    name: 'id',
    required: true,
    type: Number,
    description: 'Order id',
  })
  @Get(':id')
  async find(@Param() params): Promise<ApiResponseDto<OrderDto, any>> {
    const order = await this.orderService.findOne(params.id);
    return {
      data: order,
      metadata: {},
    };
  }
}
