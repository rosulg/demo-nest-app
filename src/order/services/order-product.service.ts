import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { OrderProduct } from '../models/order-product.entity';
import { OrderProductDto } from '../dtos/order-product.dto';

@Injectable()
export class OrderProductService {
  constructor(
    @InjectRepository(OrderProduct)
    private readonly orderProductRepository: Repository<OrderProduct>,
  ) {}

  async findAll(): Promise<OrderProductDto[]> {
    const orderProducts = await this.orderProductRepository.find();
    return OrderProductDto.fromMany(orderProducts);
  }

  async findOne(id: string): Promise<OrderProductDto> {
    const orderProduct = await this.orderProductRepository.findOne(id);
    return OrderProductDto.from(orderProduct);
  }

  async create(dto: OrderProductDto): Promise<OrderProductDto> {
    const orderProduct = dto.toEntity();
    const createdOrderProduct = await this.orderProductRepository.save(
      orderProduct,
    );
    return OrderProductDto.from(createdOrderProduct);
  }
}
