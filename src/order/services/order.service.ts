import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Order } from '../models/order.entity';
import { OrderDto } from '../dtos/order.dto';

interface Data {
  orders: OrderDto[];
  count: number;
}

@Injectable()
export class OrderService {
  constructor(
    @InjectRepository(Order)
    private readonly orderRepository: Repository<Order>,
  ) {}

  async findAll(): Promise<OrderDto[]> {
    const orders = await this.orderRepository.find();
    return OrderDto.fromMany(orders);
  }

  async findOne(id: string): Promise<OrderDto> {
    const order = await this.orderRepository.findOne(id);
    return OrderDto.from(order);
  }

  async findAndCount(limit: number, offset: number): Promise<Data> {
    const [orders, count] = await this.orderRepository.findAndCount({
      take: limit,
      skip: offset,
    });
    return {
      orders: OrderDto.fromMany(orders),
      count,
    };
  }

  async create(dto: OrderDto): Promise<OrderDto> {
    const order = dto.toEntity();
    const createdOrder = await this.orderRepository.save(order);
    return OrderDto.from(createdOrder);
  }
}
