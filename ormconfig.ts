import { ConfigService } from './src/config/services/config.service';

const config = new ConfigService();

const ormConfig = {
  ...config.typeOrmConfig,
  migrationsTableName: 'migration',
  migrations: ['src/migration/*.ts'],
  cli: {
    migrationsDir: 'src/migration',
  },
};

export = ormConfig;
