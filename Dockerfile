# Node image
FROM node:12.16.1-alpine As development

# Create app directory
WORKDIR /usr/src/app

# Copy package.json, package-lock.json and tsconfig files (tsconfig files are required for building)
COPY package*.json tsconfig.json tsconfig.build.json ./

# Install app dependencies
RUN npm install

# Copy app src and config files
COPY src/ src/
COPY ormconfig.ts config.*.env ./

# Bundle app source
RUN npm run build

# Create a new image
FROM node:12.16.1-alpine As production

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/src/app

# tsconfig files are now not needed
# COPY package*.json ./

# TODO: tsconfig.json tsconfig.build.json should be removed, this is only done to meet the requirement
# "Mock data can be executed from the command line inside the container"
COPY package*.json tsconfig.json tsconfig.build.json ./

# Comment in when requirement to run seed data inside the container is obsolete
# Only install production dependencies so the image size is smaller

RUN npm install --only=production
# TODO: --only=dev intall should be removed, this is only done to meet the requirement
# "Mock data can be executed from the command line inside the container"
RUN npm install --only=dev

# Copy app src and config files
COPY src/ src/
COPY ormconfig.ts config.*.env ./

# Copy only the dist directory
COPY --from=development /usr/src/app/dist ./dist

EXPOSE 3000
CMD ["node", "dist/main"]
