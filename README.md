# Demo Nest App

Simple REST API built with Nest

## Prerequisites for running in a Docker container

### Environment
Note that other versions might still be suitable however these were used during development
```
docker-compose version 1.24.1
Docker version 19.03.2
```

#### Running the application

**NB! All instructions are from project's root**

1. Make sure ports 3000 and 5432 are not used
2. In project's root run and wait for `docker-compose up -d db api` to finish (this will run the PostgreSQL container and the API container)
3. Run `docker ps` - make sure the containers went up and copy the `demo-api` container's `<container id>`
4. Exec into the container by running `docker exec -it <container id> /bin/sh` (if you are using Windows I recommend running it with Command Prompt)
5. Inside the docker container run `npm run db:migration:run` to create required tables etc.
6. Inside the docker container run `npm run db:seed` to generate seed data

 **API** is running at: http://localhost:3000
 **Swagger documentation** is available at: http://localhost:3000/docs
 **PostgreSQL** is running at localhost:5432
 
 If you wish to connect to the database then the default
 credentials are:
 
```
POSTGRES_PORT = 5432
POSTGRES_USER = 'postgres'
POSTGRES_PASSWORD = 'postgres'
POSTGRES_DB = 'local'
```

## Prerequisites for development

```
node v12.16.1
npm 6.13.4
docker-compose version 1.24.1
Docker version 19.03.2
```

In project's root:

1. `npm install`
2. `docker-compose up -d db` - starts PostgreSQL database in a container
2. `npm run db:migration:run` - run the migrations
3. `npm run start:dev` - starts the development server at http://localhost:3000

Some useful commands:

1. `npm run db:reset` - drops created database tables & recreates them
2. `npm run db:drop` - drops the created database tables
3. `npm run db:migration:generate` - generates required migration if something related to the database has changed
4. `npm run db:migration:run` - runs migrations
5. `npm run db:seed` - generates seed data (can be used multiple times)


